# Hooking Demonstration

This repo demonstrates hooking through `LD_PRELOAD` and `ptrace`.

## Simple Hooking

### Building

```bash
cmake -B build
cmake --build build
```

### Normal greeter execution

```bash
cmake --build build --target run_normal
```

![run_normal](/uploads/8d68a5cf31fce395c18cdf2a91114fda/run_normal.gif)

### `LD_PRELOAD` execution

We utilize `LD_PRELOAD` along with a custom `printf` function in our hooks library.

```bash
cmake --build build --target run_ld_pre_hook
```

![run_ld_pre_load](/uploads/96d80621a55695b628b7664a4a7c2d76/run_ld_pre_load.gif)

### GDB execution

We utilize GDB in batch mode to attach and subsequently override the `printf` `got.plt` entry.

```bash
cmake --build build --target run_gdb
```

![run_gdb](/uploads/15852ebafb6c6c500aca18a391425b41/run_gdb.gif)

## Manual `ptrace`-based hooking on x64

The `hooking` folder contains a complete Rust project that directly utilizes `ptrace` as well as `elf` and `/etc/pid/maps` parsing,
in order to override a function of a running process with an un-linked shared library function of the same name.

### Example Usage

First start a running process:

```bash
cmake --build build --target start_waiter
example_pid
example_pid
...
```

Then build and run the rust executable, while supplying the PID, shared library and target function.

(We use the bunyan formatter for logging formatting but that can be ignored)

```bash
cd hooking
cargo run -- --pid example_pid --module $PROJECT_ROOT/build/libhooks.so --function printf | bunyan
```

![hooking](/uploads/d7b3019c27e5322fe2b4b75a55e40aaa/hooking.gif)

### Implementation details

- Use ptrace to attach and stop process. 
- Save register and rip-pointed text section.
- Override rip text section with a syscall and `rax` jump
- Prepare registers for `mmap` syscall, we use the new page for writing payloads.
- Calculate virtual libc addresses by parsing `/proc/pid/maps`
- Write payload that calls dlopen to mmap-ed region and jump to it.
- Parse process ELF through `/proc/pid/exe` and shared library ELF to calculate `got.plt` entry and hook function addresses
- Overwrite `got.plt`
- Restore registers and text segment, jump to initial rip address and detach.
