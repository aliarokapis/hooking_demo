set unwindonsignal on
set verbose off
set pagination off
set debuginfod enabled off

b main
commands 1
set var $lib_ptr = (void*)dlopen("./build/libhooks.so", 1)
set var $printf_hook = (void*)dlsym($lib_ptr, "printf")
set $printf_got_offset = 0x2ebf
set var $printf_got = (void**)(&main + $printf_got_offset)
set *$printf_got = $printf_hook
continue
end

start
quit
