use nix::{
    errno::Errno,
    sys::{
        ptrace,
        signal::Signal::SIGTRAP,
        wait::{waitpid, WaitPidFlag, WaitStatus},
    },
    unistd::Pid,
};

#[derive(thiserror::Error, Debug)]
pub enum PtracerError {
    #[error("ptrace failed with: {0}")]
    PtraceError(#[from] Errno),
    #[error("io error: {0}")]
    Io(#[from] std::io::Error),
    #[error("wrong wait status: {0:#?}")]
    WaitError(WaitStatus),
}

pub type PtracerResult<T> = Result<T, PtracerError>;

#[derive(Debug)]
pub struct Ptracer {
    pid: Pid,
}

pub type Regs = libc::user_regs_struct;

impl Ptracer {
    pub const WORD_SIZE: usize = std::mem::size_of::<libc::c_long>();

    const _ASSERTER: [u8; (std::mem::size_of::<libc::c_long>() == std::mem::size_of::<i64>())
        as usize] = [0];

    #[tracing::instrument]
    pub fn attach(pid: Pid) -> PtracerResult<Ptracer> {
        ptrace::attach(pid)?;
        Ok(Ptracer { pid })
    }

    #[tracing::instrument(skip(self))]
    pub fn wait_stop(&self) -> PtracerResult<()> {
        match waitpid(self.pid, Some(WaitPidFlag::WSTOPPED))? {
            WaitStatus::Stopped(_, _) => Ok(()),
            status => Err(PtracerError::WaitError(status)),
        }
    }

    #[tracing::instrument(skip(self))]
    pub fn write(&self, addr: u64, data: &[u8]) -> PtracerResult<()> {
        let blocks = data.len() / Self::WORD_SIZE;
        let padding = data.len() % Self::WORD_SIZE;

        let mut target = addr;
        let mut source_index = 0;

        for _ in 0..blocks {
            unsafe {
                let value = {
                    let mut value = [0_u8; Self::WORD_SIZE];
                    value.copy_from_slice(&data[source_index..source_index + Self::WORD_SIZE]);
                    i64::from_ne_bytes(value)
                };

                ptrace::write(
                    self.pid,
                    target as *mut libc::c_void,
                    value as *mut libc::c_void,
                )?;
            };

            target += Self::WORD_SIZE as u64;
            source_index += Self::WORD_SIZE;
        }

        if padding != 0 {
            let value: i64 = {
                let mut value = ptrace::read(self.pid, target as *mut libc::c_void)?.to_ne_bytes();

                let remaining = &data[source_index..];

                value[..padding].copy_from_slice(remaining);

                i64::from_ne_bytes(value)
            };

            unsafe {
                ptrace::write(
                    self.pid,
                    target as *mut libc::c_void,
                    value as *mut libc::c_void,
                )?;
            }
        }

        Ok(())
    }

    #[tracing::instrument(skip(self))]
    pub fn read(&self, addr: u64, data: &mut [u8]) -> PtracerResult<()> {
        let blocks = data.len() / Self::WORD_SIZE;
        let padding = data.len() % Self::WORD_SIZE;

        let mut target = addr;
        let mut source_index = 0;

        for _ in 0..blocks {
            let value = ptrace::read(self.pid, target as *mut libc::c_void)?;

            data[source_index..source_index + Self::WORD_SIZE]
                .copy_from_slice(&value.to_ne_bytes());

            target += Self::WORD_SIZE as u64;
            source_index += Self::WORD_SIZE;
        }

        if padding != 0 {
            let value = ptrace::read(self.pid, target as *mut libc::c_void)?;

            data[source_index..].copy_from_slice(&value.to_ne_bytes()[..padding]);
        }

        Ok(())
    }

    #[tracing::instrument(skip(self))]
    fn wait_trap(&self) -> PtracerResult<()> {
        match waitpid(self.pid, None)? {
            WaitStatus::Stopped(_, SIGTRAP) => Ok(()),
            status => Err(PtracerError::WaitError(status)),
        }
    }

    #[tracing::instrument(skip(self))]
    pub fn get_regs(&self) -> PtracerResult<Regs> {
        Ok(ptrace::getregs(self.pid)?)
    }

    #[tracing::instrument(skip(self))]
    pub fn set_regs(&self, regs: Regs) -> PtracerResult<()> {
        Ok(ptrace::setregs(self.pid, regs)?)
    }

    #[tracing::instrument(skip(self))]
    pub fn single_step(&self) -> PtracerResult<()> {
        ptrace::step(self.pid, None)?;
        self.wait_trap()
    }

    #[tracing::instrument(skip(self))]
    pub fn cont(&self) -> PtracerResult<()> {
        ptrace::cont(self.pid, None)?;
        self.wait_trap()
    }

    #[allow(clippy::too_many_arguments)]
    #[tracing::instrument(skip(self))]
    fn call_remote(
        &self,
        remote_addr: usize,
        arg1: usize,
        arg2: usize,
        arg3: usize,
        arg4: usize,
        arg5: usize,
        arg6: usize,
    ) -> PtracerResult<usize> {
        let orig = ptrace::getregs(self.pid)?;
        let new = {
            let mut new = orig;
            new.rdi = arg1.try_into().unwrap();
            new.rsi = arg2.try_into().unwrap();
            new.rdx = arg3.try_into().unwrap();
            new.rcx = arg4.try_into().unwrap();
            new.r8 = arg5.try_into().unwrap();
            new.r9 = arg6.try_into().unwrap();
            new.rax = 0;
            new.rip = remote_addr.try_into().unwrap();
            new
        };
        ptrace::setregs(self.pid, new)?;
        ptrace::step(self.pid, None)?;
        self.wait_trap()?;
        let ret = ptrace::getregs(self.pid)?;
        ptrace::setregs(self.pid, orig)?;
        Ok(ret.rax as usize)
    }
}

impl Drop for Ptracer {
    fn drop(&mut self) {
        if let Err(e) = ptrace::detach(self.pid, None) {
            tracing::error!(
                "Ptracer failed to detach from {}, with Err: {}",
                self.pid,
                e
            );
        }
    }
}
