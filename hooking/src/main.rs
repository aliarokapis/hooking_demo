mod ptracer;

use anyhow::Context;

use clap::Parser;
use nix::unistd::Pid;
use std::path::PathBuf;

use tracing_subscriber::filter::EnvFilter;
use tracing_subscriber::layer::SubscriberExt;

use goblin::elf::{Elf, program_header::PT_LOAD};

use crate::ptracer::{Ptracer, Regs};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about=None)]
struct Args {
    /// The pid of the target process
    #[arg(short, long, value_name = "PID")]
    pid: i32,

    /// The hook module to load
    #[arg(short, long)]
    module: PathBuf,

    /// The function to hook
    #[arg(short, long)]
    function: String,
}

#[tracing::instrument]
fn get_module_base_addr(pid: Pid, pattern: &str) -> anyhow::Result<u64> {
    use proc_maps::get_process_maps;

    let module_info = get_process_maps(pid.as_raw())
        .context("Failed to get process maps.")?
        .into_iter()
        .find(|i| {
            i.filename().is_some()
                && i.filename().unwrap().to_string_lossy().contains(pattern)
        })
        .context("Failed to find module in process maps.")?;

    Ok(module_info.start() as u64)
}

#[tracing::instrument]
fn get_process_base_addr(pid: Pid) -> anyhow::Result<u64> {
    use proc_maps::get_process_maps;

    let module_info = get_process_maps(pid.as_raw())
        .context("Failed to get process maps.")?
        .into_iter()
        .next()
        .context("Failed to find module in process maps.")?;

    Ok(module_info.start() as u64)
}

#[tracing::instrument]
fn find_global_rel_addr(elf: &Elf, vaddr: u64) -> anyhow::Result<u64> {

    let hdr = elf
        .program_headers
        .iter()
        .find(|h| h.p_type == PT_LOAD && h.vm_range().contains(&(vaddr as usize)))
        .context(format!("failed to program header of relative vaddr: {}", vaddr))?;

    Ok(hdr.p_paddr + (vaddr - hdr.p_vaddr))
}

#[tracing::instrument]
fn get_remote_addr(remote_pid: Pid, pattern: &str, local_addr: u64) -> anyhow::Result<u64> {
    let local_pid = nix::unistd::getpid();
    let local_base_addr = get_module_base_addr(local_pid, pattern)?;
    let remote_base_addr = get_module_base_addr(remote_pid, pattern)?;
    Ok(local_addr + remote_base_addr - local_base_addr)
}

#[tracing::instrument]
fn find_got_plt_offset(pid: Pid, symbol: &str) -> anyhow::Result<u64> {
    let bin = std::fs::read(format!("/proc/{}/exe", pid))
        .context("failed to read the process' elf file")?;
    let elf = Elf::parse(&*bin).context("failed to parse elf file")?;

    let relocs = &elf.pltrelocs;
    let syms = &elf.dynsyms;
    let strsyms = &elf.dynstrtab;
    let (_, offset) = relocs
        .iter()
        .filter_map(|s| Some((strsyms.get_at(syms.get(s.r_sym)?.st_name)?, s.r_offset)))
        .find(|(sym, _)| *sym == symbol)
        .context("failed to find symbol in plt relocations")?;

    Ok(offset)
}

#[tracing::instrument]
fn find_hook_offset(module: &str, symbol: &str) -> anyhow::Result<u64> {
    let bin = std::fs::read(module).context("failed to read the process' elf file")?;
    let elf = Elf::parse(&*bin).context("failed to parse elf file")?;

    let strsyms = &elf.strtab;
    let syms = &elf.syms;
    let (_, _, offset) = syms
        .into_iter()
        .filter_map(|s| Some((strsyms.get_at(s.st_name)?, s.is_function(), s.st_value)))
        .find(|(name, is_func, _)| *name == symbol && *is_func)
        .context("failed to find symbol in plt relocations")?;

    let rel_addr = find_global_rel_addr(&elf, offset).context("failed to find global symbol address")?;

    Ok(rel_addr)
}

fn main() -> anyhow::Result<()> {
    init_tracing().context("Failed to initialize tracing.")?;

    let args = Args::parse();

    let pid = Pid::from_raw(args.pid);

    let module = args.module.to_str().unwrap();

    let function = args.function;

    inject(pid, module, &function).map_err(|e| {
        tracing::error!("Injection failed: {:?}", e);
        e
    })
}

fn inject(pid: Pid, module: &str, function: &str) -> anyhow::Result<()> {
    let tracer = Ptracer::attach(pid)?;

    tracer.wait_stop()?;

    let register_backup = tracer.get_regs()?;

    const PAGE_SIZE: u64 = 0x1000;

    // prepare mmap system call

    let new_regs = Regs {
        rax: libc::SYS_mmap as u64,
        rdi: 0, // address
        rsi: PAGE_SIZE,
        rdx: (libc::PROT_READ | libc::PROT_EXEC) as u64,
        r10: (libc::MAP_PRIVATE | libc::MAP_ANONYMOUS) as u64,
        r8: u64::MAX, // fd
        r9: 0,        // offset
        ..register_backup
    };

    const WORD_SIZE: usize = Ptracer::WORD_SIZE;

    let mut text_backup = [0_u8; WORD_SIZE];

    let payload_1: [u8; WORD_SIZE] = [
        0x0f, 0x05, // syscall
        0xff, 0xe0, // jmp %rax
        0x00, 0x00, 0x00, 0x00,
    ];

    let rip = register_backup.rip;

    tracing::info!("rip before jump: {:#x}", rip);

    tracer
        .read(rip, &mut text_backup)
        .context("failed to read text backup.")?;
    tracing::debug!("previous text was read as: {:x?}", text_backup);

    tracing::debug!("writing: {:x?} as payload", payload_1);

    tracer
        .write(rip, &payload_1)
        .context("failed to write text.")?;

    let mut test_payload = text_backup;
    tracer
        .read(rip, &mut test_payload)
        .context("failed to read test text.")?;

    tracing::debug!("written-payload was read as: {:x?}", test_payload);

    if test_payload != payload_1 {
        anyhow::bail!("Failed to read written payload");
    }

    tracer
        .set_regs(new_regs)
        .context("failed to set mmap registers.")?;

    tracer
        .single_step()
        .context("failed while single stepping.")?;

    let new_regs = tracer
        .get_regs()
        .context("failed to read post-mmap registers.")?;

    let mmap_memory_addr = new_regs.rax;

    tracing::info!("allocated memory at {:#x}", mmap_memory_addr);
    tracing::info!("executing jump to mmap region");

    tracer
        .single_step()
        .context("failed while single stepping.")?;

    let new_regs = tracer
        .get_regs()
        .context("failed to read post-jump registers.")?;

    tracing::info!("rip after jump: {:#x}", new_regs.rip);

    if new_regs.rip == mmap_memory_addr as u64 {
        tracing::debug!("successfully jumped to mmap region");
    } else {
        anyhow::bail!("jumped to unexpected region");
    }

    let (payload_2, offset) = {
        const LIBC_PATTERN: &str = "/libc";
        let their_dlopen = get_remote_addr(pid, LIBC_PATTERN, libc::dlopen as usize as u64)
            .context("failed to get remote dlopen address.")?;

        let delta: [u8; 4] = {
            const REL32_SIZE: isize = 5;
            let delta = their_dlopen as isize - mmap_memory_addr as isize - REL32_SIZE;
            let delta: i32 = delta
                .try_into()
                .context(format!("cannot relative jump of {}", delta))?;
            delta.to_ne_bytes()
        };

        let module_cstr = std::ffi::CString::new(module).unwrap();

        let mut payload_2: Vec<u8> = vec![
            0x48, 0x83, 0xe4, 0xf0, // and rsp, -16 => ensure aligned stack
            0xe8, delta[0], delta[1], delta[2], delta[3], // call rel32
            0xcc,     // trap
        ];

        let offset = payload_2.len();

        payload_2.extend(module_cstr.to_bytes());

        (payload_2, offset)
    };

    tracing::info!("writing payload to mmap_address");
    tracing::debug!("payload_2: {:x?}", payload_2);

    tracer
        .write(mmap_memory_addr, &payload_2)
        .context("Failed to write second payload to mmap-ed memory.")?;

    // prepare dlopen registers

    let new_regs = Regs {
        rax: 0,
        rdi: mmap_memory_addr + offset as u64,
        rsi: libc::RTLD_NOW as u64,
        ..new_regs
    };

    tracer
        .set_regs(new_regs)
        .context("Failed to set registers for dlopen")?;

    tracer.cont().context("Failed on cont.")?;

    let new_regs = tracer
        .get_regs()
        .context("Failed to get registers after trap")?;

    // prepare for return to rip

    let new_regs = Regs {
        rax: rip as u64,
        ..new_regs
    };

    let payload_3: [u8; Ptracer::WORD_SIZE] = [
        0xff, // jmp to rax
        0xe0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    ];

    tracing::info!("writing jump to rax payload.");
    tracing::debug!("payload_3: {:x?}", payload_3);

    tracer
        .write(new_regs.rip, &payload_3)
        .context("Failed to write jmp to rax payload.")?;

    tracer
        .single_step()
        .context("Failed while single stepping.")?;

    tracing::info!("writing original text at rip");
    tracer
        .write(rip, &text_backup)
        .context("Failed to write original text back at rip")?;

    tracing::info!("restore original registers");
    tracer
        .set_regs(register_backup)
        .context("Failed to restore original registers")?;

    let process_base_addr = get_process_base_addr(pid)?;
    let got_plt_offset = find_got_plt_offset(pid, function)?;
    let overwrite_addr = process_base_addr + got_plt_offset;

    let module_base_addr = get_module_base_addr(pid, module)?;
    let hook_offset = find_hook_offset(module, function)?;
    let source_addr = (module_base_addr + hook_offset).to_ne_bytes();

    tracing::debug!("
    process_base_addr: {:x} \n
    got_plt_offset: {:x} \n
    overwrite_addr: {:x} \n
    module_base_base: {:x} \n
    hook_offset: {:x} \n
    source_addr: {:x} \n
    ",

    process_base_addr,
    got_plt_offset,
    overwrite_addr,
    module_base_addr,
    hook_offset,
    u64::from_ne_bytes(source_addr),
    );

    tracing::debug!(
        "writing hook address {:x} to got.plt entry at {:x}",
        u64::from_ne_bytes(source_addr),
        overwrite_addr
    );

    tracer
        .write(overwrite_addr, &source_addr)
        .context("Failed to overwrite got.plt entry")?;

    Ok(())
}

fn init_tracing() -> anyhow::Result<()> {
    let env_filter = EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::new("info"));

    let formatting_layer =
        tracing_bunyan_formatter::BunyanFormattingLayer::new("hooking".into(), std::io::stdout);

    let storage_layer = tracing_bunyan_formatter::JsonStorageLayer;

    let subscriber = tracing_subscriber::Registry::default()
        .with(env_filter)
        .with(storage_layer)
        .with(formatting_layer);

    tracing::subscriber::set_global_default(subscriber).context("Failed to set subscriber")?;

    Ok(())
}
