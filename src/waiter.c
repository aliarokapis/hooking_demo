#include <stdio.h>
#include <unistd.h>

int main()
{
    int pid = getpid();

    while (1)
    {
        printf("%d\n", pid);
        sleep(1);
    }
}
