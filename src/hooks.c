#include <dlfcn.h>
#include <stdio.h>

static int (*old_printf)(const char * filename, ...) = 0;
int printf (const char *__restrict __format, ...)
{
    return (*old_printf)("hooked!\n");
}

static void __attribute__((constructor)) attach()
{
    old_printf = dlsym(RTLD_NEXT, "printf");
}
